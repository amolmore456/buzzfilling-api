const mysql = require('mysql');

const connection = mysql.createPool({
    connectinLimit:50,
    host:'localhost',
    port:'3306',
    user:'root',
    password:'root',
    database:'customers'
});

module.exports = connection;