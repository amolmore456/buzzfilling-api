const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const customers = require('./api/routes/customer');
const user = require('./api/routes/user');

app.use(morgan('dev')); 
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json()); 
app.use(cors());
// app.use((req,res,next)=>{
//     res.header('Access-Control-Allow-Origin','http://localhost:4200');
//     res.header('Access-Control-Allow-Header','Content-Type,Accept,Authorization');
//     if(req.method === 'OPTIONS'){
//         res.header('Access-Control-Allow-Methods','GET,POST,DELETE,PUT');
//         return res.status(200).json({});
//     }    
//     next(); 
// });
// This is api to as localhost:3000/mail 

app.use('/customer',cors(),customers);
app.use('/user',cors(),user);

app.use((req,res,next)=>{
    const error =  new Error('Not Found, Please Check the API again');
    error.status = 404;
    next(error);
});

app.use((error,req,res,next)=>{
    res.status(error.status || 500);
    res.json ({
        error:{
            message:error.message
        }
    });
})

module.exports = app;