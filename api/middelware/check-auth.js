const jwt = require('jsonwebtoken');
module.exports = (req,res,next) => {
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token,"=#Qu!43cj@rrwjVJ8dq!mT6@sa7?8VygUN$ZFJM8zcS!emz=*dHTPwCPVyNQRM?S$P--mwS5g*vdHD*%2gqAB-3uC2pKJ&g_k-!JfAHP4+L");
        req.userData = decoded;
        next();
    }catch(error){
        return res.status(401).json({
            message:"Authentication Fail",
            error:error
        });
    }
        
}