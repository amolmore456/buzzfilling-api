const express = require('express');
const router = express.Router();

const connection = require('../../config/connection');

const checkAuth = require('../middelware/check-auth');


router.post('/',(req,res,next)=>{
    fname = req.body.fname;
    lname = req.body.lname;
    fname= fname +" "+ lname;
    const details = {
            fname:fname,
            email:req.body.email,
            phone:req.body.phone,
            city:req.body.city,
            servicecategory:req.body.serviceCategory,
            servicetype:req.body.serviceType,
            address:req.body.address
    }
    connection.getConnection((error,tempConnect)=>{
        if(error){
            tempConnect.release();
            console.log(error);
        }
        else {
            sql = "Insert INTO customer SET ?";
            tempConnect.query(sql,details,(error,result)=>{
                tempConnect.release();
                if(error){
                    console.log("Error in the query"+ error);
                    // JSON RESPONCE
                    res.status(404).json({
                        message:error
                    })
                }
                else {
                    // JSON RESPONCE
                    res.status(200).json({
                        message:"Thank You, We will contact you soon."
                    });
                }
            });
        }
    })    
});

router.get('/all',checkAuth,(req,res,next)=>{
    connection.getConnection((error,tempConnect)=>{
        if(error){
            tempConnect.release();
            console.log(error);
        }
        else {
            sql = "SELECT * FROM customer";
            tempConnect.query(sql,(error,result)=>{
                tempConnect.release();
                if(error){
                    // JSON RESPONCE
                    res.status(200).json({
                        responce:error
                    })
                }
                else {
                    // JSON RESPONCE
                    res.json({
                        responce:result
                    });
                }
            });
        }
    })    
});

router.get('/:id',checkAuth,(req,res,next)=>{
    cId = req.params.id;
    connection.getConnection((error,tempConnect)=>{
        if(error){
            tempConnect.release();
            console.log(error);
        }
        else {
            sql = "SELECT * FROM customer WHERE cId = ?";
            tempConnect.query(sql,cId,(error,result)=>{
                tempConnect.release();
                if(error){
                    console.log("Error in the query"+ error);
                    // JSON RESPONCE
                    res.json({
                        responce:error
                    })
                }
                else {
                    // JSON RESPONCE
                    res.json({
                        responce:result[0]
                    });
                }
            });
        }
    })    
});
   


router.delete('/:id',checkAuth,(req,res,next)=>{
    cId = req.params.id;
    connection.getConnection((error,tempConnect)=>{
        if(error){
            tempConnect.release();
            console.log(error);
        }
        else {
            sql = "Delete FROM customer WHERE cId = ?";
            tempConnect.query(sql,cId,(error,result)=>{
                tempConnect.release();
                if(error){
                    console.log("Error in the query"+ error);
                    // JSON RESPONCE
                    res.json({
                        responce:error
                    })
                }
                else {
                    // JSON RESPONCE
                    res.json({
                        responce:"Deleted Successfully."
                    });
                }
            });
        }
    })    
});

module.exports = router;