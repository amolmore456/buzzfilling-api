const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const connection = require('../../config/connection');
const salt=10;

router.get('/',(req,res)=>{
    res.status(200).json({
        message:"It worked"
    });
});
router.post('/signUp',(req,res,next)=>{
    bcrypt.hash(req.body.password,salt,(err,hash)=>{
        if(err){
            return res.status(500).json({
                error:err
            })
        }
        else {
            const user = {
                uname:req.body.uname,
                password:hash,
                email:req.body.email,
                role:req.body.role
            }
            console.log(user);
            connection.getConnection((error,tempConnect)=>{
                if(error){
                    tempConnect.release();
                    console.log(error.message);
                }
                else {
                    sql = "Insert INTO users SET ?";
                    tempConnect.query(sql,user,(error,result)=>{
                        tempConnect.release();
                        if(error){
                            console.log("Error in the query"+ error);
                            // JSON RESPONCE
                            res.json({
                                responce:error.sqlMessage
                            })
                        }
                        else {
                            // JSON RESPONCE
                            res.json({
                                responce:"User Created Successfully.",
                                uname:user.uname
                            });
                        }
                    });
                }
            })   
        }
    });
})

router.post('/signIn',(req,res,next)=>{
    uname = req.body.uname;
    password =req.body.password;
    // console.log(password);  
    connection.getConnection((error,tempConnect)=>{
        if(error){
            tempConnect.release();
            console.log(error);
        }
        else {
            sql = `Select * FROM users WHERE uname = "${uname}"`;
            tempConnect.query(sql,(error,result)=>{
                tempConnect.release();
                if(error){
                    console.log("Error in the query"+ error);
                    // JSON RESPONCE
                    res.json({
                        responce:error.sqlMessage
                    })
                }
                else {
                    if(result[0] == null){
                        res.status(404).json({
                            message:"User dose not exist.",
                            code:"404"
                        })
                    }
                    else {
                        // Extract the password
                        hash = result[0].password;
                        user = result[0].uname;
                        console.log(user);
                        
                        bcrypt.compare(password,hash,(err,succ)=>{
                            console.log(succ)
                            if(succ === false){
                                res.status(401).json({
                                    message:"Username and Password not match",
                                    code:"401"
                                })
                            }
                            else if(succ === true) {
                                // Payload
                                const payload ={
                                    email:result[0].email,
                                    uId:result[0].uId,
                                    uname:result[0].uname,
                                    role:result[0].role
        
                                }
                                // Send token
                                const token = jwt.sign(payload, "=#Qu!43cj@rrwjVJ8dq!mT6@sa7?8VygUN$ZFJM8zcS!emz=*dHTPwCPVyNQRM?S$P--mwS5g*vdHD*%2gqAB-3uC2pKJ&g_k-!JfAHP4+L",{ expiresIn: 60 *60 });
                                res.json({
                                    token :token,
                                    message: "Successful",
                                    code:200
                                })    
                            }
                        })    
                    }
                }                                      
            });
        }
    }) 
})




module.exports = router;