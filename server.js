const fs = require('fs');
// const http = require('http');
const https =  require('https');
const privateKey  = fs.readFileSync('encryption/taxfina.key','utf8');
const certificate = fs.readFileSync('encryption/taxfina.crt','utf8');
const credentials = {key: privateKey, cert: certificate};
const app = require('./app');

const port = process.env.PORT || 3000;

const server =  https.createServer(credentials,app);

server.listen(port);